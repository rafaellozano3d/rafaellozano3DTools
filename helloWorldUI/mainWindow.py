"""
Created on 26 jul. 2017
@author: rafaellozano3d.com/devBlog
"""

from PySide2 import QtUiTools
from PySide2.QtGui import QWindow
import os
import functools

"""
Establecemos el directorio de la interfaz ui,
con este metodo lo buscara en el mismo sitio
donde se este ejecutando el .py, dentro de la
carpeta ui
"""

ui_main_file = os.path.join(os.path.dirname(__file__), "ui/main.ui")

"""
Definimos nuestra clase
"""


class MainWindow(QWindow):
    def __init__(self, parent=None):
        """
        Con estos parametros conectaremos la interfaz con nuestro codigo, en principio
        siempre utilizaremos estas mismas lineas asi que nos limitaremos a copiarlo
        """

        """
        constructor
        :param parent
        """

        super(MainWindow, self).__init__(parent)
        loader = QtUiTools.QUiLoader()
        self.ui = loader.load(ui_main_file)

        """
        EStablecemos el nombre de nuestra ventana
        """

        self.ui.setWindowTitle('Tool Hola Mundo')

        """
        Y por ultimo, con esta funcion, leeremos
        lo que el usuario va haciendo con nuestra
        tool, como por ejemplo, pulsar el boton
        de hello world
        """

        self.connect_signals()

    """
    Sera aqui donde definamos que sucedera cuando
    el usuario realice una determinada accion
    """
    def connect_signals(self):
        """
        En nuestro caso, definimos que cuando el usuario haga click en el boton helloWorldButton
        que hemos definido, se ejecute la funcion __escribirMensaje
        """
        self.ui.helloWorldButton.clicked.connect(
            functools.partial(self.__imprimeMensaje, "Hello World!"))

    """
    Definimos la funcion de imprimeMensaje, que se
    limitara a imprimir por la consola de comandos
    el mensaje Hello World que le hemos enviado
    """
    def __imprimeMensaje(self, s_message):
            print " {} ".format(s_message)

    """
    La funcion run es necesaria para que nuestra
    tool comience su ejecucion
    """
    def run(self):
        self.ui.show()
