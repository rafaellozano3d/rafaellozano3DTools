"""
Created on 27 jul. 2017
@author: rafaellozano3d.com/devBlog
"""

from PySide2 import QtUiTools
from PySide2.QtGui import QWindow
import os
import maya.cmds as cmds

"""
Establecemos el directorio de la interfaz ui,
con este metodo lo buscara en el mismo sitio
donde se este ejecutando el .py, dentro de la
carpeta ui
"""
ui_main_file = os.path.join(os.path.dirname(__file__), "ui/main.ui")

"""
Definimos nuestra clase
"""
class MainWindow(QWindow):
    def __init__(self, parent=None):
        """
        Con estos parametros conectaremos la
        interfaz con nuestro codigo, en principio
        siempre utilizaremos estas mismas lineas
        asi que nos limitaremos a copiarlo
        """
        """
        constructor
        :param parent
        """
        super(MainWindow, self).__init__(parent)
        loader = QtUiTools.QUiLoader()
        self.ui = loader.load(ui_main_file)

        """
        EStablecemos el nombre de nuestra ventana
        """
        self.ui.setWindowTitle('Exportar por tipos')

        """
        Y por ultimo, con esta funcion, leeremos
        lo que el usuario va haciendo con nuestra
        tool, como por ejemplo, pulsar el boton
        de hello world
        """
        self.connect_signals()

    """
    Sera aqui donde definamos que sucedera cuando
    el usuario realice una determinada accion
    """
    def connect_signals(self):
        """
        En nuestro caso, definimos que cuando el
        usuario haga click en el boton helloWorldButton
        que hemos definido, se ejecute la funcion
        __escribirMensaje
        """
        self.ui.exportButton.clicked.connect(self.__exportar)

    """
    Definimos la funcion de exportar, mediante la cual
    comprobaremos los checkbox y, en caso de estar
    seleccionados, agregamos ese tipo de elemento
    a nuestra seleccion. Ademas del flag type existen
    otros muchos que podeis ver en:
    http://download.autodesk.com/us/maya/2011help/CommandsPython/ls.html
    """
    def __exportar(self):
            if(self.ui.geometriesCheckBox.isChecked()):
                geometrias = cmds.ls(type="mesh")
                cmds.select(geometrias, add=True)
            if(self.ui.lightsCheckBox.isChecked()):
                luces = cmds.ls(type="light")
                cmds.select(luces, add=True)
            if(self.ui.nurbsCheckBox.isChecked()):
                curvas = cmds.ls(type="nurbsCurve")
                cmds.select(curvas, add=True)
            if(self.ui.jointCheckBox.isChecked()):
                joints = cmds.ls(type="joint")
                cmds.select(joints, add=True)
            if(self.ui.camerasCheckBox.isChecked()):
                camaras = cmds.ls(type="camera")
                cmds.select(camaras, add=True)
            """
            Por ultimo, mostramos al usuario el mensaje
            de guardar fichero, para que elija donde,
            con el flag sff activado, que asegura que
            se esta guardando la seleccion actual
            """
            basicFilter = "*.ma"
            nombreFichero = cmds.fileDialog2(
                fileFilter=basicFilter, sff=True, dialogStyle=2)
            """Mediante la funcion fileDialog2
            preguntamos la informacion del nombre y
            donde guardarlo al usuario, que sera
            utilizada en la funcion file, donde se
            da paso a guardar el fichero
            """
            cmds.file(nombreFichero[0], type='mayaAscii',
                      exportAsReference=True)

    """
    La funcion run es necesaria para que nuestra
    tool comience su ejecucion
    """
    def run(self):
        self.ui.show()
