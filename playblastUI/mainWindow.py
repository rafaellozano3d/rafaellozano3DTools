"""
Created on 27 jul. 2017
@author: rafaellozano3d.com/devBlog
"""
from PySide2 import QtUiTools
from PySide2.QtGui import QWindow
import os
import maya.cmds as cmds
import functools

"""
Establecemos el directorio de la interfaz ui,
con este metodo lo buscara en el mismo sitio
donde se este ejecutando el .py, dentro de la
carpeta ui
"""
ui_main_file = os.path.join(os.path.dirname(__file__), "ui/main.ui")

"""
Definimos nuestra clase
"""
class MainWindow(QWindow):
    def __init__(self, parent=None):
        """
        Con estos parametros conectaremos la
        interfaz con nuestro codigo, en principio
        siempre utilizaremos estas mismas lineas
        asi que nos limitaremos a copiarlo
        """
        """
        constructor
        :param parent
        """
        super(MainWindow, self).__init__(parent)
        loader = QtUiTools.QUiLoader()
        self.ui = loader.load(ui_main_file)

        """
        EStablecemos el nombre de nuestra ventana
        """
        self.ui.setWindowTitle('Playblast Tool')

        """
        Y por ultimo, con esta funcion, leeremos
        lo que el usuario va haciendo con nuestra
        tool, como por ejemplo, pulsar el boton
        de hello world
        """
        self.connect_signals()

    """
    Sera aqui donde definamos que sucedera cuando
    el usuario realice una determinada accion
    """
    def connect_signals(self):
        """
        Todo el mecanismo de nuestra tool se centra
        en rescatar el valor que se encuentre en los
        cuadro de texto que haya introducido el
        usuario, y hacer un playblast desde el origen
        hasta el fin, enlazamos el boton y despues
        definimos la funcion
        """
        self.ui.playblastButton.clicked.connect(
            functools.partial(self.__hacerPlayblast))

    """
    Aqui sera donde definamos la funcion
    En primer lugar, creamos dos variables donde
    guardar los valores introducidos por el usuario.
    Para ello, necesitamos indicar que estamos
    buscando un elemento de la self.ui, llamado
    frameInicialText o frameFinalText, y agregar al
    final un .toPlainText(), una funcion propia de
    python que te transforma el valor a texto plano
    """
    def __hacerPlayblast(self):
        frameInicio = self.ui.frameInicialText.toPlainText()
        frameFin = self.ui.frameFinalText.toPlainText()
        # Por ultimo ejecutamos el comando para que
        # genere el playblast
        cmds.playblast(st=frameInicio, et=frameFin, format="qt",
                       clearCache=1, viewer=1, showOrnaments=1, fp=4,
                       percent=100, compression="H.264", quality=100,
                       width=1280, height=720)

    """
    La funcion run es necesaria para que nuestra
    tool comience su ejecucion
    """
    def run(self):
        self.ui.show()