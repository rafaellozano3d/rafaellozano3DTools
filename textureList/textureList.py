# Creamos los imports necesarios
import maya.cmds as cmds
import maya.app.general.fileTexturePathResolver as ftpr

# Guardamos todos los ficheros tipo textura dentro de una variable
l_files = cmds.ls(type="file")

"""
Recorremos la variable para cada uno de los ficheros, el objetivo
principal sera el analisis del atributo "useFrameExtension", el
cual indica si es una textura animada, y el "uvTilingMode", que nos
dira si se trata de unas texturas sencillas o de tipo UDIM, segun si
su valor es de 0 o 3.
"""
for s_file in l_files:
    print "\n", s_file, "\n---------------\n"

    # Lo primero sera rescatar los valores de estos atributos, asi como
    # el directorio del fichero
    imagePath = cmds.getAttr(s_file + ".fileTextureName")
    tiling_mode = cmds.getAttr(s_file + ".uvTilingMode")
    frame_extension = cmds.getAttr(s_file + ".useFrameExtension")

    # Si frame_extension ha almacenado un 1, significa que la textura
    # esta animada
    if frame_extension == 1:
        print("Type: Animated texture\n")

        # Obtenemos la plantilla que sigue Maya para detectar el conjunto
        # de ficheros que forman la animacion, es decir, que parte del nombre
        # se repite
        pattern = ftpr.getFilePatternString(imagePath, True, False)

        # Almacenamos todos los elementos que siguen esa plantilla
        fileList = ftpr.findAllFilesForPattern(pattern, None)

        # Y, por ultimo, para poder indicar en que frame se visualiza esa
        # textura, almacenamos el offset, por lo que partiremos del frame 0
        # y sumaremos el valor del offset para indicar correctamente en que
        # frame se encuentra
        offset = cmds.getAttr(s_file + ".frameOffset")
        counter = 0

        # Imprimimos por pantalla el valor del frame mas el offset, y la ruta
        # del fichero. Para acceder al elemento bastara con acceder a la
        # posicion que apunta el counter, ya que este incrementa de uno en uno
        # y parte de 0
        for frameImage in fileList:
            print "Frame [", (counter+int(offset)), "]:", fileList[counter]
            counter = counter + 1
    else:
        # En el caso de que no sea animada, y su uvTilingMode sea 0, significa
        # que se trata de una textura normal, por lo que simplemente imprimimos
        # por pantalla su ruta
        if tiling_mode == 0:
            print("Type: Simple texture\n")
            print imagePath

        # Si se trata de una textura UDIM, es decir, con el uvTilingMode
        # igual a 3
        if tiling_mode == 3:
            print("Type: UDIM texture\n")

            # Obtenemos una vez mas la parte que se repite en sus nombres
            udim_frame_extension = cmds.getAttr(s_file + ".frameExtension")
            pattern = ftpr.getFilePatternString(imagePath, True, False)
            fileList = ftpr.findAllFilesForPattern(pattern, None)
            counter = 0

            # E imprimimos los nombres de los ficheros que nos ha devuelto
            for frameImage in fileList:
                print fileList[counter]
                counter = counter + 1